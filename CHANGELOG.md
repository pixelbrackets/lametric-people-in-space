# Changelog

2020-09-26 Dan Untenzu <mail@pixelbrackets.de>

  * 1.3.0
  * FEATURE Docs: Change demo URL

2020-04-20 Dan Untenzu <mail@pixelbrackets.de>

  * 1.2.0
  * FEATURE Add help page
  * FEATURE Add CI

2020-03-30 Dan Untenzu <mail@pixelbrackets.de>

  * 1.1.0
  * FEATURE Get latest data via API

2020-03-30 Dan Untenzu <mail@pixelbrackets.de>

  * 1.0.0
  * FEATURE Inital version
