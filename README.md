# LaMetric People in Space App

[![Build Status](https://img.shields.io/gitlab/pipeline/pixelbrackets/lametric-people-in-space?style=flat-square)](https://gitlab.com/pixelbrackets/lametric-people-in-space/pipelines)
[![Made With](https://img.shields.io/badge/made_with-php-blue?style=flat-square)](https://gitlab.com/pixelbrackets/lametric-people-in-space#requirements)
[![License](https://img.shields.io/badge/license-gpl--2.0--or--later-blue.svg?style=flat-square)](https://spdx.org/licenses/GPL-2.0-or-later.html)

This app shows how many humans are in space right now on a LaMetric device.

🚀 A build of the app frontend is available in the [LaMetric App Store](https://developer.lametric.com/applications/info/21641)

🚀 A build of the app backend is available as [Webapp](https://lametric-people-in-space.app.pixelbrackets.de/)

This repository contains the sourcecode to build the app.

![Demo](./docs/screen.png)

## Requirements

- PHP

## Installation

`composer install`

## Source

https://gitlab.com/pixelbrackets/lametric-people-in-space

## License

GNU General Public License version 2 or later

The GNU General Public License can be found at http://www.gnu.org/copyleft/gpl.html.

## Author

Dan Untenzu (<mail@pixelbrackets.de> / [@pixelbrackets](https://pixelbrackets.de))

## Changelog

See [./CHANGELOG.md](CHANGELOG.md)

## Contribution

This script is Open Source, so please use, patch, extend or fork it.
