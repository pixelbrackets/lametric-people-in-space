<?php

require __DIR__ . '/../vendor/autoload.php';

$template = new \Pixelbrackets\Html5MiniTemplate\Html5MiniTemplate();
$template->setTitle('🚀 LaMetric People in Space App');
$template->setStylesheet('skeleton');
$template->setStylesheetMode(\Pixelbrackets\Html5MiniTemplate\Html5MiniTemplate::STYLE_INLINE);
$template->setContent('<h1>🚀 LaMetric People in Space App</h1>
<p>
    1. Connect a phone with a LaMetric<br>
    2. Add <a href="https://developer.lametric.com/applications/info/21641">People in Space</a> App from Appstore
</p>');
echo $template->getMarkup();
