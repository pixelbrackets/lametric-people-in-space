<?php

require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;

$client = new Client();

// get latest data from open notify API
$response = $client->request(
    'GET',
    'http://api.open-notify.org/astros.json'
);
$humansInSpace = json_decode((string) $response->getBody());

// push data to devices
$requestUrl = getenv('LAMETRICURL');
$requestToken = getenv('LAMETRICTOKEN');
$requestBody = [
    'frames' =>
        [
            0 =>
                [
                    'text' => $humansInSpace->number,
                    'icon' => 'i1749',
                    'index' => 0,
                ],
        ],
];

$response = $client->request(
    'POST',
    $requestUrl,
    [
        'headers' => [
            'X-Access-Token' => $requestToken,
        ],
        'json' => $requestBody
    ]
);
